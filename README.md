# Edgelinux 2.0

* [Introduction](#introduction)
* [Quickstart](#quickstart)
* [Modification](#modification)
* [Troubleshooting](#troubleshooting)

## Introduction

For [Digital Rebar Provision](http://rebar.digital), which is maintained by
[RackN](https://rackn.com).

This content package provides a workflow, stage, bootenv and kickstart template to use for installing Edgelinux 2.0 on targeted machine(s). Edgelinux is the customized CentOS 7 configuration provided by [Antsle.com](https://antsle.com/) to run their hypervisor management tool, [antMan](https://antsle.com/products/antmanedgelinux/#0).

## Quickstart
Issue the following commands from your drp server:

1. git clone and cd into the content directory: `cd content`
2. Build the bundle; outputs edgelinux-2.0.yml to the root of the git project.
`drpcli contents bundle ../edgelinux-2.0.yml`
3. Upload the bundle to the drp endpoint
`drpcli contents create ../edgelinux-2.0.yml`
4. Upload and explode the ISO so that the kernel, initrd, packages etc are made available from the DRP server for the installation
`drpcli bootenvs uploadiso edgelinux-2.0.yml`
5. Discover a Machine and throw the workflow against it!

## Modification
You can easily configure the provided yaml and kickstart template to suite your needs. After editing them to your satisfaction, you can update the content pack in place by:
`drpcli contents bundle ../edgelinux-2.0.yml`
and
`drpcli contents update edgelinux-2.0 ../edgelinux-2.0.yml`

Content bundles are meant to be read-only; if you find yourself wanting to edit them (for example, editing the select-kickseed param to point at a cloned kickseed template that you named something else), you can convert the bundle into individual parts in DRP's data store to make them writable. By doing this, you can no longer manage the components as a bundle; you'd have to remove the bootenv, stage, workflow and template separately if you wanted to remove all of the content.

Uploading and breaking up the bundle into separate (and editable) parts:
`drpcli contents convert ../edgelinux-2.0.yml`

## Troubleshooting
The 'bootenvs uploadiso' step from step 4 of the quickstart should have done the following:
1. Made an ISO file named `edgeLinux-2.0.0.iso` into the /path/to/drp-dir/tftpboot/isos/ directory, where tftpboot is relative to the DRP installation directory (I.E. /var/lib/dr-provision/tftpboot/isos/edgeLinux-2.0.0.iso)
2. Unpacked the ISO into /path/to/drp-dir/tftpboot/centos-7-everything/install/ directory

If the ISO did not explode into tftpboot (causing PXE boot to fail to find the initrd/kernel), you can use the following to manually unpack it if you're in the tftpboot directory:
`./explode_iso.sh edgelinux-2.0 /var/lib/dr-provision/tftpboot/ /var/lib/dr-provision/tftpboot/isos/edgeLinux-2.0.0.iso /var/lib/dr-provision/tftpboot/edgelinux-2.0/install 0d35cf4fd177eeff0a494d00c54a2b9924c9c175f4a981e504fd5354559beb59`

If your `drpcli contents create` command did not successfully upload the .yml bundle, try creating the bundle in JSON format with the following command (.json extension signals drpcli output the bundle in json format):

`drpcli contents bundle ../edgelinux-2.0.json`

The run the similar following command to upload the .json bundle:
`drpcli contents create ../edgelinux-2.0.json`
